#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include <sys/types.h>
#include <fcntl.h>


void seek_write(int fd, off_t pos, const char* c){
  lseek(fd, pos, SEEK_SET);
  write(fd, c, 1);
}

char seek_read(int fd, off_t pos) {
  lseek(fd, pos, SEEK_SET);
  char c;
  read(fd, &c, 1);
  return c;
}

int main(int argc, char** arv){
  int fd = open("./dummy", O_RDWR);

  if (fd < 0) {
    printf("Error\n");
    exit(EXIT_FAILURE);
  }
  pid_t pid = fork();
  if (pid == 0) { // child
    char *c = "f";
    seek_write(fd, 0, c);
    printf("Child read pos 1:%c\n", seek_read(fd, 1));
  }
  else if (pid > 0) { // parent
    char *c = "g";
    seek_write(fd, 1, c);
    printf("Parent read pos 0:%c\n", seek_read(fd, 0));
  }
  else {
    printf("Error\n");
    exit(EXIT_FAILURE);
  }
  return 0;
}
