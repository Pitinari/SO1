-module(server).
-export([iniciar_servidor/0, servidor/1]).

servidor(List) -> 
  receive 
    {susc, PId} -> servidor(lists:append(List, [PId]));
    {mensaje, MSG} -> lists:foreach(List, fun(A) -> A ! MSG end),
                      servidor(List);
    {desusc, PId} -> servidor(lists:delete(PId, List));
    _ -> servidor(List)
  end.

iniciar_servidor() -> 
  servidor([]).