-module(sync).
-export([createLock/0, lock/1, unlock/1, destroyLock/1]).
-export([createSem/1, semP/1, semV/1, destroySem/1]).
-export([testLock/0, testSem/0]).
-export([locker/1, semaphore/1]).

locker (L) -> 
  case L of
    [] -> receive
            {lock, PId} -> PId ! up,
                           locker(lists:append(L, [PId]));
            destroy -> die;
            _ -> locker(L)
          end;
    [Hd] -> receive
              {lock, PId} -> locker(lists:append(L, [PId]));
              {unlock, PId} -> if
                                 Hd == PId -> locker([]);
                                 true -> locker(L)
                               end;
              destroy -> die;
              _ -> locker(L)
            end;
    [Fst, Snd | Tail] -> receive
                                {lock, PId} -> locker(lists:append(L, [PId]));
                                {unlock, PId} -> if
                                                   Fst == PId -> Snd ! up,
                                                                 locker([Snd | Tail]);
                                                   true -> locker(L)
                                                 end;
                                destroy -> die;
                                _ -> locker(L)
                            end
  end.

createLock () -> 
  spawn(?MODULE, locker, [[]]).

lock (L) -> 
  L ! {lock, self()},
  receive
    up -> ok
  end.

unlock (L) -> 
  L ! {unlock, self()}.

destroyLock (L) -> 
  L ! destroy.

semaphore (S) ->   
  case S of
    {0, T} -> receive
                {wait, PId} -> semaphore({0, lists:append(T, [PId])});
                post -> case T of
                          [] -> {1, T};
                          [Hd | Tail] -> Hd ! up,
                                         semaphore({0, Tail})
                        end;
                destroy -> die;
                _ -> semaphore(S) 
              end;
    {N, T} -> receive 
                {wait, PId} -> PId ! up,
                               semaphore({N-1, T});
                post -> semaphore({N+1, T});
                destroy -> die;
                _ -> semaphore(S)
              end
  end.

createSem (N) -> 
  spawn(?MODULE, semaphore, [{N, []}]).

destroySem (S) ->
  S ! destroy.

semP (S) -> 
  S ! {wait, self()},
  receive
    up -> ok
  end.

semV (S) -> 
  S ! post.

f (L, W) ->
  lock(L),
  % regioncritica(),
  io:format("uno ~p~n", [self()]),
  io:format("dos ~p~n", [self()]),
  io:format("tre ~p~n", [self()]),
  io:format("cua ~p~n", [self()]),
  unlock(L),
  W ! finished.

waiter (L, 0) -> destroyLock(L);
waiter (L, N) -> receive finished -> waiter(L, N-1) end.

waiter_sem (S, 0) -> destroySem(S);
waiter_sem (S, N) -> receive finished -> waiter_sem(S, N-1) end.

testLock () ->
  L = createLock(),
  W = spawn(fun () -> waiter(L, 3) end),
  spawn (fun () -> f(L, W) end),
  spawn (fun () -> f(L, W) end),
  spawn (fun () -> f(L, W) end),
  ok.

sem (S, W) ->
  semP(S),
  %regioncritica(), bueno, casi....
  io:format("uno ~p~n", [self()]),
  io:format("dos ~p~n", [self()]),
  io:format("tre ~p~n", [self()]),
  io:format("cua ~p~n", [self()]),
  io:format("cin ~p~n", [self()]),
  io:format("sei ~p~n", [self()]),
  semV(S),
  W ! finished.

testSem () ->
  S = createSem(2), % a lo sumo dos usando io al mismo tiempo
  W = spawn (fun () -> waiter_sem (S, 5) end),
  spawn (fun () -> sem (S, W) end),
  spawn (fun () -> sem (S, W) end),
  spawn (fun () -> sem (S, W) end),
  spawn (fun () -> sem (S, W) end),
  spawn (fun () -> sem (S, W) end),
  ok.