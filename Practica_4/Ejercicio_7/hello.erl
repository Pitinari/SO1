-module(hello).
-export([init/0, hello_aux/1, hello/0]).

hello_aux(PId) ->
  link(PId),
  hello().

hello() ->
  receive after 1000 -> ok end,
  io:fwrite("Hello ~p~n", [case rand:uniform(10) of 10 -> 1/uno; _ -> self() end]),
  hello().

revive() ->
    receive
        {'EXIT', _From, _Reason} ->
          spawn(?MODULE, hello_aux, [self()]),
            revive()
    end.

init() -> 
  spawn(?MODULE, hello_aux, [self()]),
  process_flag(trap_exit, true),
  revive().