-module(ring).
-export([init/1, ring/2, ringer/1, mess/2]).

ringer(PId) ->
  % io:fwrite("Me:~p!Partner:~p!~n", [self(), PId]),
  receive
    {msg, N} -> io:fwrite("~p: I received ~p!~n", [self(), N]),
                case N of
                  0 -> PId ! exit;
                  N -> PId ! {msg, (N - 1)}
                end,
                ringer(PId);
    exit -> PId ! exit %, io:fwrite("~p: dying~n", [self()])
  end.

ring(N, First) ->
  case N of 
    1 -> ringer(First);
    N -> PId = spawn(?MODULE, ring, [(N-1), First]),
         ringer(PId)
  end.

mess(PId, N) ->
  PId ! {msg, N}.

init(N) -> 
  spawn(?MODULE, mess, [self(), N]),
  ring(N, self()).