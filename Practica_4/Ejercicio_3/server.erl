-module(server).
-export([init_server/0, serv/0, wait_connect/2, handle_conn/3, request_parser/3, tickets/1]).

init_server() ->
    serv().

serv() ->
    case gen_tcp:listen(3000, [{reuseaddr, true}, {active, false}]) of
            {error, Reason} -> {error, Reason};
            {ok, ListenSocket} -> io:fwrite("SERVER CREATED~n"),
                                  TicketerID = spawn(?MODULE, tickets, [0]),
        	                      wait_connect(ListenSocket, TicketerID)
    end.


wait_connect(ListenSocket, TicketerID) ->
	{ok, Socket} = gen_tcp:accept(ListenSocket),
    io:fwrite("SOCKET CREATED~n", []),
	spawn(?MODULE, handle_conn, [Socket, TicketerID, ""]),
    wait_connect(ListenSocket, TicketerID).

handle_conn(Sock, TicketerID, AccumulatedRequest) ->
    case gen_tcp:recv(Sock, 0) of
        {ok, Msg} ->    io:fwrite("MESSAGE RECEIVED ~p~n",[Msg]),
                        handle_conn(Sock, TicketerID, request_parser(Sock, TicketerID, AccumulatedRequest ++ Msg));
        {error, enotconn} ->    io:fwrite("SOCKET CERRADO~n"),
                                gen_tcp:close(Sock);
        {error, closed} ->  io:fwrite("SOCKET CERRADO~n"),
                            gen_tcp:close(Sock);
        _ ->    handle_conn(Sock, TicketerID, AccumulatedRequest)
    end.

request_parser(Sock, TicketerID, Request) ->
    ParsedRequest = string:split(Request, "\n"),
    case length(ParsedRequest) of
        2 ->    Line = lists:nth(1,ParsedRequest),
                case Line of
                    "NUEVO" ->   TicketerID ! {nuevo, self()},
                                receive
                                    {ok, N} -> gen_tcp:send(Sock, list_to_binary(integer_to_list(N)));
                                    _ -> error
                                end;
                    "CHAU" ->   gen_tcp:close(Sock),
                                io:fwrite("SOCKET CERRADO~n"),
                                exit("SOCKET CERRADO");
                    _ -> io:fwrite("MENSAJE ERRONEO~n")
                end,
                LeftoverRequest = lists:nth(2,ParsedRequest),
                ReturnMsg = request_parser(Sock, TicketerID, LeftoverRequest);
        1 ->    io:fwrite("MENSAJE INCOMPLETO~n"),
                ReturnMsg = Request
    end,
    ReturnMsg.

tickets(N) ->
    receive
        {nuevo, PID} -> PID ! {ok, N},
                        tickets(N+1);
        _ -> error
    end.
