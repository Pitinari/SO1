-module(temp).
-export([wait/1, test/0, cronometro/3]).

wait(N) ->
  receive
    %%
  after
    N -> ok
  end.

cronometro(Fun, Hasta, Periodo) ->
  Fun(),
  if 
    Hasta > Periodo -> receive after Periodo -> cronometro(Fun, Hasta - Periodo, Periodo) end;
    true -> receive after Periodo -> fin end
  end.

test() ->
  spawn(?MODULE, cronometro, [fun () -> io:fwrite("Tick~n") end, 60000, 5000]).