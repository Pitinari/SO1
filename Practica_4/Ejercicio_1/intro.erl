-module(intro).
-export([init/0, string_test/0]).

match_test () ->
	{A,B} = {5,4}, %valido, A = 5. B = 4
	% {C,C} = {5,4}, %invalido, no se puede redefinir un valor
	{B,A} = {4,5}, %valido A = 5, B = 4
	{D,D} = {5,5}.

string_test () -> [
	helloworld == 'helloworld', % true, se usan '' para definir atomos
	"helloworld" < 'helloworld', % false
	helloworld == "helloworld", % false helloworld es atomo y "helloworld" es un string
	[$h,$e,$l,$l,$o,$w,$o,$r,$l,$d] == "helloworld", % true, los strings son listas de caracteres
	[104,101,108,108,111,119,111,114,108,100] < {104,101,108,108,111,119,111,114,108,100}, % false, son los mismos caracteres
	[104,101,108,108,111,119,111,114,108,100] > 1, % true 
	[104,101,108,108,111,119,111,114,108,100] == "helloworld"]. % true, los caracteres se transforman a numero binario y luego a decimal

tuple_test (P1, P2) ->
	io:fwrite("El nombre de P1 es ~p y el apellido de P2 es ~p~n", [nombre(P1), apellido(P2)]).

nombre(P) -> 
	case P of
		{persona, {nombre, N}, {apellido, _A}} -> N
	end.

apellido(P) ->
	case P of
		{persona, {nombre, _N}, {apellido, A}} -> A
	end.
	
filtrar_por_apellido(Personas, Apellido) -> 
	case Personas of 
		[] -> [];
		[Hd | Tail] -> case Hd of 
							   		 {persona, {nombre, _Nom}, {apellido, Ape}} ->
												if 
													Ape == Apellido -> [Hd | filtrar_por_apellido(Tail, Apellido)];
												  true -> filtrar_por_apellido(Tail, Apellido)
												end
									 end
	end.

init () ->
	P1 = {persona, {nombre, "Juan"}, {apellido, "Gomez"}},
	P2 = {persona, {nombre, "Carlos"}, {apellido, "Garcia"}},
	P3 = {persona, {nombre, "Javier"}, {apellido, "Garcia"}},
	P4 = {persona, {nombre, "Rolando"}, {apellido, "Garcia"}},
	match_test(),
	tuple_test(P1, P2),
	string_test(),
	Garcias = filtrar_por_apellido([P4, P3, P2, P1], "Garcia").
