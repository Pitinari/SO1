#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include "queue.h"
#include <semaphore.h>
#define LECTORES 5
#define ESCRITORES 5
#define ARRLEN 10240

struct rw_lock{                // lock
  pthread_mutex_t queueLock;   // mutex para modificar la requestsQueue
  struct Queue *requestsQueue; // queue que guarda las requests
  int mode;                    // estado actual del lock: 0 nothing, 1 reading, 2 writing
  int currentLectors;
};

struct request{
  sem_t *sem; // semaforo para despertar el hilo
  int type;   // 1 read request, 2 write request
};

struct rw_lock lock;

int arr[ARRLEN];

void rw_lock_init (struct rw_lock *arrLock) {
  pthread_mutex_init(&arrLock->queueLock, NULL);
  arrLock->requestsQueue = createQueue();
  arrLock->mode = 0;
  arrLock->currentLectors = 0;
}

void lock_lectors(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->queueLock);
  if (arrLock->mode == 0) {
    arrLock->currentLectors += 1;
    arrLock->mode = 1;
    pthread_mutex_unlock(&arrLock->queueLock);
    return;
  }
  if (arrLock->mode == 1 && emptyQueue(arrLock->requestsQueue)) {
    arrLock->currentLectors += 1;
    pthread_mutex_unlock(&arrLock->queueLock);
    return;
  }
  struct request* req = malloc(sizeof(struct request));
  sem_t *sem = malloc(sizeof(sem_t));
  sem_init(sem, 0, 0);
  req->sem = sem;
  req->type = 1;
  enQueue(arrLock->requestsQueue, (void *)req);
  pthread_mutex_unlock(&arrLock->queueLock);

  sem_wait(sem);
  sem_destroy(sem);
  free(sem);

  pthread_mutex_lock(&arrLock->queueLock);
  struct request *first = firstQueue(arrLock->requestsQueue);
  if (first != NULL) {
    if (first->type == 1){
      first = deQueue(arrLock->requestsQueue);
      arrLock->currentLectors += 1;
      sem_post(first->sem);
      free(first);
    }
  }
  pthread_mutex_unlock(&arrLock->queueLock);
  return;
}

void lock_writers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->queueLock);
  if (arrLock->mode == 0) {
    arrLock->mode = 2;
    pthread_mutex_unlock(&arrLock->queueLock);
    return;
  }

  struct request* req = malloc(sizeof(struct request));
  sem_t *sem = malloc(sizeof(sem_t));
  sem_init(sem, 0, 0);
  req->sem = sem;
  req->type = 2;
  enQueue(arrLock->requestsQueue, (void *)req);
  pthread_mutex_unlock(&arrLock->queueLock);

  sem_wait(sem);
  sem_destroy(sem);
  free(sem);
  return;
}

void unlock_lectors(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->queueLock);
  arrLock->currentLectors--;
  if (arrLock->currentLectors) {
    pthread_mutex_unlock(&arrLock->queueLock);
    return;
  }
  struct request *req = (struct request*) deQueue(arrLock->requestsQueue);
  if (req == NULL) {
    arrLock->mode = 0;
  }
  else {
    if (req->type == 1) arrLock->currentLectors += 1;
    sem_post(req->sem);
    arrLock->mode = req->type;
    free(req);
  }
  pthread_mutex_unlock(&arrLock->queueLock);
  return;
}

void unlock_writers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->queueLock);
  struct request *req = deQueue(arrLock->requestsQueue);
  if (req == NULL) {
    arrLock->mode = 0;
    pthread_mutex_unlock(&arrLock->queueLock);
  }
  else {
    if (req->type == 1) arrLock->currentLectors += 1;
    sem_post(req->sem);
    arrLock->mode = req->type;
    free(req);
    pthread_mutex_unlock(&arrLock->queueLock);
  }
  return;
}

void *escritor(void *arg) {
  int i;
  int num = arg - (void*)0;
  while (1) {
    // sleep(random() % 3);
    lock_writers(&lock);
    printf("Escritor %d escribiendo\n", num);
    for (i = 0; i < ARRLEN; i++) arr[i] = num;
    unlock_writers(&lock);
  }
  return NULL;
}

void *lector(void *arg) {
  int v, i;
  int num = arg - (void*)0;
  while (1) {
    // sleep(random() % 3);
    lock_lectors(&lock);
    v = arr[0];
    for (i = 1; i < ARRLEN; i++) {
      if (arr[i] != v) break;
    }
    if (i < ARRLEN) printf("Lector %d, error de lectura\n", num);
    else printf("Lector %d, dato %d\n", num, v);
    unlock_lectors(&lock);
  }
  return NULL;
}

int main() {
  pthread_t lectores[LECTORES], escritores[ESCRITORES];
  rw_lock_init(&lock);
  int i;
  for (i = 0; i < LECTORES; i++)
    pthread_create(&lectores[i], NULL, lector, i + (void*)0);
  for (i = 0; i < ESCRITORES; i++)
    pthread_create(&escritores[i], NULL, escritor, i + (void*)0);
  pthread_join(lectores[0], NULL); /* Espera para siempre */
  return 0;
}
