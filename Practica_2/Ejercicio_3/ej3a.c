#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#define LECTORES 5
#define ESCRITORES 5
#define ARRLEN 10240

struct rw_lock{
  pthread_mutex_t readWrite;
  pthread_mutex_t lockViewers;
  int currentLectors;
};

struct rw_lock lock;

int arr[ARRLEN];

void rw_lock_init (struct rw_lock *arrLock) {
  pthread_mutex_init(&arrLock->readWrite, NULL);
  pthread_mutex_init(&arrLock->lockViewers, NULL);
  arrLock->currentLectors = 0;
}

void lock_lectors(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->lockViewers);
  if(arrLock->currentLectors == 0){
    pthread_mutex_lock(&arrLock->readWrite);
  }
  arrLock->currentLectors++;
  pthread_mutex_unlock(&arrLock->lockViewers);
}

void unlock_lectors(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->lockViewers);
  arrLock->currentLectors--;
  if(arrLock->currentLectors == 0){
    pthread_mutex_unlock(&arrLock->readWrite);
  }
  pthread_mutex_unlock(&arrLock->lockViewers);
}

void lock_writers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->readWrite);
}

void unlock_writers(struct rw_lock *arrLock) {
  pthread_mutex_unlock(&arrLock->readWrite);
}

void * escritor(void *arg) {
  int i;
  int num = arg - (void*)0;
  while (1) {
    sleep(random() % 3);
    lock_writers(&lock);
    printf("Escritor %d escribiendo\n", num);
    for (i = 0; i < ARRLEN; i++)
      arr[i] = num;
    unlock_writers(&lock);
  }
  return NULL;
}

void * lector(void *arg) {
  int v, i;
  int num = arg - (void*)0;
  while (1) {
    sleep(random() % 3);
    lock_lectors(&lock);
    v = arr[0];
    for (i = 1; i < ARRLEN; i++) {
      if (arr[i] != v) break;
    }
    if (i < ARRLEN) printf("Lector %d, error de lectura\n", num);
    else printf("Lector %d, dato %d\n", num, v);
    unlock_lectors(&lock);
  }
  return NULL;
}

int main() {
  pthread_t lectores[LECTORES], escritores[ESCRITORES];
  rw_lock_init(&lock);
  int i;
  for (i = 0; i < LECTORES; i++)
    pthread_create(&lectores[i], NULL, lector, i + (void*)0);
  for (i = 0; i < ESCRITORES; i++)
    pthread_create(&escritores[i], NULL, escritor, i + (void*)0);
  pthread_join(lectores[0], NULL);/* Espera para siempre */
  return 0;
}
