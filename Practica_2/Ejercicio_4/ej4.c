/*4 (Problema del Barbero, Dijkstra). Una barber´ıa tiene una sala de espera con N sillas y un
Pr´actica 2 - Sincronizaci´on Avanzada P´agina 2
Sistemas Operativos I
barbero. Si no hay clientes para atender, el barbero se pone a dormir. Si un cliente llega y todas las sillas
est´an ocupadas, se va. Si el barbero est´a ocupado pero hay sillas disponibles, se sienta en una y espera a
ser atendido. Si el barbero est´a dormido, despierta al barbero. El cliente y el barbero deben ejecutar concurrentemente las funciones me_cortan() y cortando() y al terminar los dos ejecutar concurrentemente
pagando() y me_pagan().
Escriba un programa que coordine el comportamiento del barbero y los clientes y expl´ıquelo*/

#include <semaphore.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

typedef struct _local {
  int clientesN, libreN, atendidoId;
  pthread_mutex_t puertaM, corteM, atendidoM, pagaM;
  pthread_cond_t esperandoC, libreC, corteC, pagaC;
  sem_t atendidoS, pagaS, esperandoS;
} local;

local barberia;

void cortando(int id) {
  printf("Barbero: Le estoy cortando al %d cliente\n", id);
  sleep(1);
  sem_post(&barberia.atendidoS);
}

void me_pagan(int id){
  sem_wait(&barberia.pagaS);
  printf("Barbero: El cliente %d me pago\n", id);
}

void *barbero(void *arg) {
  while (1) {
    sem_wait(&barberia.esperandoS);
    cortando(barberia.atendidoId);
    me_pagan(barberia.atendidoId);
  }
}

void me_cortan(int id){
  printf("%d: Me cortan\n", id);
  sem_wait(&barberia.atendidoS);
}

void pagando(int id) {
  printf("%d: Pagando\n", id);
  sleep(1);
  sem_post(&barberia.pagaS);
}

void *cliente(void *arg) {
  int id = (int)arg;
  pthread_mutex_lock(&barberia.puertaM);
  if (barberia.clientesN < 10) {
    barberia.clientesN++;
    pthread_mutex_unlock(&barberia.puertaM);
    pthread_mutex_lock(&barberia.corteM);
    barberia.atendidoId = id;
    sem_post(&barberia.esperandoS);
    me_cortan(id);
    pagando(id);
    pthread_mutex_unlock(&barberia.corteM);
    pthread_mutex_lock(&barberia.puertaM);
    barberia.clientesN--;
    pthread_mutex_unlock(&barberia.puertaM);
  }
  else {
    pthread_mutex_unlock(&barberia.puertaM);
  }
  return NULL;
}

void *puerta(void *arg) {
  int id = 0;
  while (1) {
    sleep(rand() % 20);
    pthread_t pCliente;
    pthread_create(&pCliente, NULL, cliente, (void *)id);
    id++;
  }
}

int main() {
  pthread_t pBarb, pPuerta;
  pthread_mutex_init(&barberia.puertaM, NULL);
  barberia.clientesN = 0;
  barberia.libreN = 1;
  sem_init(&barberia.atendidoS, 0, 0);
  sem_init(&barberia.pagaS, 0, 0);

  pthread_create(&pBarb, NULL, barbero, NULL);
  pthread_create(&pPuerta, NULL, puerta, NULL);
  pthread_join(pBarb, NULL);
}