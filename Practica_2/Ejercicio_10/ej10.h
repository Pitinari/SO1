#include <semaphore.h>
#include <pthread.h>

typedef struct _Cond {
  int durmN;
  sem_t sem;
  pthread_mutex_t mutex;
} Cond;

void cond_init(Cond *condition);

void cond_wait(Cond *condition, pthread_mutex_t *lock);

void cond_signal(Cond *condition);

void cond_broadcast(Cond *condition);

void cond_destroy(Cond *condition);
