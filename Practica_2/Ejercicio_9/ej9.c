#include <pthread.h>

typedef struct _Sem {
  pthread_mutex_t mutex;
  pthread_cond_t up;
  int cont;
} Sem;

void sem_init(Sem *semaphore, int initial) {
  semaphore->cont = initial;
  pthread_mutex_init(&semaphore->mutex, NULL);
  pthread_cond_init(&semaphore->up, NULL);
}

void sem_post(Sem *semaphore) {
  pthread_mutex_lock(&semaphore->mutex);
  semaphore->cont++;
  pthread_cond_broadcast(&semaphore->up);
  pthread_mutex_unlock(&semaphore->mutex);
}

void sem_wait(Sem *semaphore) { 
  pthread_mutex_lock(&semaphore->mutex);
  while (semaphore->cont == 0) {
    pthread_cond_wait(&semaphore->up, &semaphore->mutex);
  }
  semaphore->cont--; 
  pthread_mutex_unlock(&semaphore->mutex);
}

void sem_destroy(Sem *semaphore) {
  pthread_mutex_destroy(&semaphore->mutex);
  pthread_cond_destroy(&semaphore->up);
}