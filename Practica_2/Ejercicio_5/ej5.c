#define B 2
#define K (2*B)

const int WAIT = 5000;
volatile int s, r, buf[B];

static inline int diff() { return (K + s - r) % K; }

void * prod(void *arg) {
  int cur = 0;
  while (1) {
    while (diff() == B);
    buf[s % B] = cur++;
    s = (s+1) % K;
  }
}

void * cons(void *arg) {
  int cur;
  while (1) {
    while (diff() == 0);
    cur = buf[r % B];
    r = (r+1) % K;
    printf("Got %d\n", cur);
  }
}