#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define M 5
#define N 5
#define SZ 8

sem_t libre, ocupado;
pthread_mutex_t bufLock;

/*
 * El buffer guarda punteros a enteros, los
 * productores los consiguen con malloc() y los
 * consumidores los liberan con free()
 */
int *buffer[SZ];
int existe[SZ] = {0};

void enviar(int *p) {
	sem_wait(&libre);
  pthread_mutex_lock(&bufLock);
  int i = 0;
  for ( ; existe[i]; i++);
  existe[i] = 1;
  buffer[i] = p;
  pthread_mutex_unlock(&bufLock);
  sem_post(&ocupado);
	return;
}

int *recibir() {
	sem_wait(&ocupado);
  pthread_mutex_lock(&bufLock);
  int i = 0;
  for ( ; !existe[i]; i++);
  existe[i] = 0;
  int *p = buffer[i];
  pthread_mutex_unlock(&bufLock);
  sem_post(&libre);
	return p;
}

void *prod_f(void *arg) {
	int id = arg - (void*)0;
	while (1) {
		sleep(random() % 3);

		int *p = malloc(sizeof *p);
		*p = random() % 100;
		printf("Productor %d: produje %p->%d\n", id, p, *p);
		enviar(p);
	}
	return NULL;
}

void *cons_f(void *arg) {
	int id = arg - (void*)0;
	while (1) {
		sleep(random() % 3);

		int *p = recibir();
		printf("Consumidor %d: obtuve %p->%d\n", id, p, *p);
		free(p);
	}
	return NULL;
}

int main() {
	pthread_t productores[M], consumidores[N];
	sem_init(&libre, 0, SZ);
	sem_init(&ocupado, 0, 0);
  pthread_mutex_init(&bufLock, NULL);
  int i;
	for (i = 0; i < M; i++)
		pthread_create(&productores[i], NULL, prod_f, i + (void*)0);

	for (i = 0; i < N; i++)
		pthread_create(&consumidores[i], NULL, cons_f, i + (void*)0);

	pthread_join(productores[0], NULL); /* Espera para siempre */
	return 0;
}