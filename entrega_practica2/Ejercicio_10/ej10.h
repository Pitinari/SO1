#include <semaphore.h>
#include <pthread.h>

typedef struct _Cond {
  int sleeping; // cantidad de threads durmiendo
  sem_t sem; // semaforo para despertar threads
  pthread_mutex_t mutex; // mutex para modificar esta estructura
} Cond;

void cond_init(Cond *condition);

void cond_wait(Cond *condition, pthread_mutex_t *lock);

void cond_signal(Cond *condition);

void cond_broadcast(Cond *condition);

void cond_destroy(Cond *condition);
