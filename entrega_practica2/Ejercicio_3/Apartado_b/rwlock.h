#include <pthread.h>

struct rw_lock{
  pthread_mutex_t readWriteLock; // mutex oara leer/escribir
  pthread_mutex_t readLock; // mutex de lectores
  pthread_cond_t cond; // variable de condicion para despertar lectores
  int currentReaders; // contador de lectores
  int writeRequest; // contador de escritores queriendo escribir
};

void rw_lock_init(struct rw_lock *lock);

void lock_writers(struct rw_lock *arrLock);

void lock_readers(struct rw_lock *arrLock);

void unlock_writers(struct rw_lock *arrLock);

void unlock_readers(struct rw_lock *arrLock);
