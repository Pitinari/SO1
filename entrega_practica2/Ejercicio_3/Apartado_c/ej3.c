#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "rwlock.h"
#define LECTORES 5
#define ESCRITORES 5
#define ARRLEN 10240

int arr[ARRLEN];

struct rw_lock lock;

void * escritor(void *arg) {
  int i;
  int num = arg - (void*)0;
  while (1) {
    sleep(random() % 3);
    lock_writers(&lock);
    printf("Escritor %d escribiendo\n", num);
    for (i = 0; i < ARRLEN; i++)
      arr[i] = num;
    unlock_writers(&lock);
  }
  return NULL;
}

void * lector(void *arg) {
  int v, i;
  int num = arg - (void*)0;
  while (1) {
    sleep(random() % 3);
    lock_readers(&lock);
    v = arr[0];
    for (i = 1; i < ARRLEN; i++) {
      if (arr[i] != v) break;
    }
    if (i < ARRLEN) printf("Lector %d, error de lectura\n", num);
    else printf("Lector %d, dato %d\n", num, v);
    unlock_readers(&lock);
  }
  return NULL;
}

int main() {
  pthread_t lectores[LECTORES], escritores[ESCRITORES];
  rw_lock_init(&lock);
  int i;
  for (i = 0; i < LECTORES; i++)
    pthread_create(&lectores[i], NULL, lector, i + (void*)0);
  for (i = 0; i < ESCRITORES; i++)
    pthread_create(&escritores[i], NULL, escritor, i + (void*)0);
  pthread_join(lectores[0], NULL);/* Espera para siempre */
  return 0;
}
