#include <stdlib.h>
#include <pthread.h>
#include "rwlock.h"


void rw_lock_init (struct rw_lock *arrLock) {
  pthread_mutex_init(&arrLock->queueLock, NULL);
  arrLock->requestsQueue = createQueue();
  arrLock->mode = 0;
  arrLock->currentReaders = 0;
}

void lock_readers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->queueLock);
  if (arrLock->mode == 0) {
    arrLock->mode = 1;
    arrLock->currentReaders = 1;
    pthread_mutex_unlock(&arrLock->queueLock);
    return;
  }
  if (arrLock->mode == 1 && emptyQueue(arrLock->requestsQueue)) {
	  arrLock->currentReaders++;
	  pthread_mutex_unlock(&arrLock->queueLock);
    return;
  }
  struct request* req = malloc(sizeof(struct request));
  pthread_cond_t* cond = malloc(sizeof(pthread_cond_t));
  pthread_cond_init(cond, NULL);
  req->cond = cond;
  req->type = 1;
  enQueue(arrLock->requestsQueue, (void *)req);

  pthread_cond_wait(cond, &arrLock->queueLock);
  free(cond);
  struct request *first = firstQueue(arrLock->requestsQueue);
  if (first != NULL) {
    if (first->type == 1){
      first = deQueue(arrLock->requestsQueue);
      arrLock->currentReaders++;
      pthread_cond_signal(first->cond);
      free(first);
    }
  }
  pthread_mutex_unlock(&arrLock->queueLock);
  return;
}

void lock_writers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->queueLock);
  if (arrLock->mode == 0) {
    arrLock->mode = 2;
    pthread_mutex_unlock(&arrLock->queueLock);
    return;
  }
  struct request* req = malloc(sizeof(struct request));
  pthread_cond_t* cond = malloc(sizeof(pthread_cond_t));
  pthread_cond_init(cond, NULL);
  req->cond = cond;
  req->type = 2;
  enQueue(arrLock->requestsQueue, (void *)req);
  pthread_cond_wait(cond, &arrLock->queueLock);
  free(cond);
  pthread_mutex_unlock(&arrLock->queueLock);

  return;
}

void unlock_writers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->queueLock);
  struct request *req = deQueue(arrLock->requestsQueue);
  if (req == NULL) {
    arrLock->mode = 0;
  }
  else {
    if(req->type == 1) arrLock->currentReaders++;
    pthread_cond_signal(req->cond);
    arrLock->mode = req->type;
  }
  pthread_mutex_unlock(&arrLock->queueLock);
  return;
}


void unlock_readers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->queueLock);
  arrLock->currentReaders--;
  if(arrLock->currentReaders != 0){
  	pthread_mutex_unlock(&arrLock->queueLock);
	  return;
  }
  struct request *req = (struct request*) deQueue(arrLock->requestsQueue);
  if (req == NULL) {
    arrLock->mode = 0;
  }
  else {
    pthread_cond_signal(req->cond);
    arrLock->mode = req->type;
  }
  pthread_mutex_unlock(&arrLock->queueLock);
  return;
}
