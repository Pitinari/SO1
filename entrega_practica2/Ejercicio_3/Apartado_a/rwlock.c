#include <stdlib.h>
#include <pthread.h>
#include "rwlock.h"

void rw_lock_init (struct rw_lock *arrLock) {
  pthread_mutex_init(&arrLock->readWriteLock, NULL);
  pthread_mutex_init(&arrLock->readLock, NULL);
  arrLock->currentReaders = 0;
}

void lock_readers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->readLock);
  if(arrLock->currentReaders == 0){
    pthread_mutex_lock(&arrLock->readWriteLock);
  }
  arrLock->currentReaders++;
  pthread_mutex_unlock(&arrLock->readLock);
}

void unlock_readers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->readLock);
  arrLock->currentReaders--;
  if(arrLock->currentReaders == 0){
    pthread_mutex_unlock(&arrLock->readWriteLock);
  }
  pthread_mutex_unlock(&arrLock->readLock);
}

void lock_writers(struct rw_lock *arrLock) {
  pthread_mutex_lock(&arrLock->readWriteLock);
}

void unlock_writers(struct rw_lock *arrLock) {
  pthread_mutex_unlock(&arrLock->readWriteLock);
}
