#include <pthread.h>

struct rw_lock{
  pthread_mutex_t readWriteLock; // mutex para leer/escribir
  pthread_mutex_t readLock; // mutex de lectores
  int currentReaders; // contador de lectores
};

void rw_lock_init(struct rw_lock *lock);

void lock_writers(struct rw_lock *arrLock);

void lock_readers(struct rw_lock *arrLock);

void unlock_writers(struct rw_lock *arrLock);

void unlock_readers(struct rw_lock *arrLock);
