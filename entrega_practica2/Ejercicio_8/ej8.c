#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct _Channel {
  int v; // valor a transmitir
  sem_t writable, readable; // writable: un lector se presento y quiere que escriban
                            // readable: un escritor escribio
  pthread_mutex_t mutex; // mutex para que no entren mas de una pareja de escritores y lectores
} Channel;

void channel_init(Channel *c) {
  sem_init(&c->writable, 0, 0);
  sem_init(&c->readable, 0, 0);
  pthread_mutex_init(&c->mutex, NULL);
  return;
}

void chan_write(Channel *c, int v) {
  sem_wait(&c->writable);
  c->v = v;
  sem_post(&c->readable);
  return;
}

int chan_read(Channel *c) {
  pthread_mutex_lock(&c->mutex);
  sem_post(&c->writable);
  sem_wait(&c->readable);
  int ret = c->v;
  pthread_mutex_unlock(&c->mutex);
  return ret;
}
