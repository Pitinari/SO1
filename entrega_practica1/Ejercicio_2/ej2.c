#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<limits.h>
#include <sys/types.h>
#include <sys/wait.h>

const int INITIAL_ALLOC_STRING = 10;

char *stdin_dynamic (){
  char *string = malloc(INITIAL_ALLOC_STRING * sizeof(char));
  int cont = 0;
  char c;
  for(;(c = getchar()) != '\n'; cont++){
    if((cont % 10) == 0){
      string = realloc(string, cont+10);
    }
    string[cont] = c;
  }
  if((cont % 10) == 0){
    string = realloc(string, cont+1);
  }
  string[cont] = '\0';
  return string;
}

char *copy_word(int *i, int *j, char *string){
	char *word = malloc(((*j) - (*i) + 1)* sizeof(char));
	int idx = 0;
	if(string[*i] == ' '){
		(*i)++;
	}
	for(; (*i) < (*j); (*i)++){
		word[idx++] = string[*i];
	}
	word[idx] = '\0';
	return word;
}

char **command_and_args(char *line) { // recibe una cadena y la transforma en un arreglo de cadenas
	int idx = 0; // cabezal de la linea
	int lastSpace = 0; // posicion del inicio o del ultimo espacio
	int cant = 0; // cantidad de strings en el arreglo
	char **palabras = malloc(5 * sizeof(char*)); // arreglo de las cadenas
	while(line[idx] != '\0'){ // hasta encontrar el fin de la cadena
		if(line[idx] == ' '){ // si se encuentra un espacio
			if((idx - lastSpace) < 2 ) { // si el ultimo espacio y el indice diferencian en menos de 2 i.e. "  "
				lastSpace = idx; // cambiamos el ultimo espacio al indice
				idx++; // aumentamos el indice
				break;
			}
			if((cant % 5) == 0){ // si
				palabras = realloc(palabras, (cant+5) * sizeof(char*));
			}
			palabras[cant] = copy_word(&lastSpace, &idx, line);
			cant++;
		}
		idx++; // si no se encuentra un espacio se lee el siguiente caracter
	}

	if((cant % 5) == 0 || ((cant+1) % 5) == 0){
		palabras = realloc(palabras, (cant+2) * sizeof(char*));
	}
	if(line[idx-1] != ' '){
		palabras[cant] = copy_word(&lastSpace, &idx, line);
		palabras[cant+1] =  NULL;
	}
	else {
		palabras[cant] =  NULL;
	}
	return palabras;
}

int has_pipe (char **commads){
  if (commads[0][0] == '|') {
    printf("Syntax error\n");
    return -1;
  }
  for (int pos = 1; commads[pos] ; pos++){
    if(commads[pos][0] == '|'){
      return pos;
    }
  }
  return 0;
}

void free_memory(char *line, char **commands, int cond){
	free(line);
	if(commands){
		char **command = commands;
		for(; *command; command++){
			free(*command);
		}
		if (cond > 0) {
			command++;
			for(; *command; command++){
				free(*command);
			}
		}
		free(commands);
	}
}

int main() {
	int fd[2];
	int wstatus;
	char *commandLine, **commandWithArgs;
	while(1){
		printf(">> ");
		fflush(stdin);
		commandLine = stdin_dynamic();
		if(strcmp(commandLine, "exit") == 0){
			free_memory(commandLine, NULL,0);
			return 0;
		}
		commandWithArgs = command_and_args(commandLine);


		int cond = has_pipe(commandWithArgs);
		if(cond == -1){
			free_memory(commandLine, commandWithArgs,cond);
			continue;
		} else if(cond){
			free(commandWithArgs[cond]);
			commandWithArgs[cond] = NULL;

		}
		// TODO: change dir on cd chdir()
		pid_t pid1 = fork();
		if(!pid1){
			pipe(fd);
			if(cond){
				pid_t pid2 = fork();
				if(!pid2){ // second command
					close(1); // close stdout
					dup(fd[1]); // pipe 1 to stdout
					close(fd[0]); // close pipe 0
					execvp(commandWithArgs[0], commandWithArgs);
					exit(1);
				}
				close(0); // close stdin
				dup(fd[0]); // pipe 0 to stdin
				close(fd[1]); // close pipe 1
				// execl("/bin/grep","/bin/grep", "WIFSIGNALED", NULL);
				execvp(commandWithArgs[cond+1], commandWithArgs+cond+1);
				exit(1);
			}
			else {
				execvp(commandWithArgs[0], commandWithArgs);
			}
		}
		wait(&wstatus);
		free_memory(commandLine, commandWithArgs, cond);
	}
	return 1;
}