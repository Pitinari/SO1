#include <stdio.h>
#include <mpi.h>

void main(int argc, char **argv)
{
  int rank, send, recv, size, sum;
  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  sum = rank;
  send = rank;
  if (rank == 0) {
    printf("Suma esperada: %d\n", ((size * (size -1)))/2);
    for (int i = 0; i < size - 1; i++) {
      MPI_Send(&send, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);
      MPI_Recv(&recv, 1, MPI_INT, size - 1, 0, MPI_COMM_WORLD, &status);
      sum += recv;
      send = recv;
    }
    
  }
  else {
    for (int i = 0; i < size - 1; i++) {
      MPI_Recv(&recv, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD, &status);
      sum += recv;
      MPI_Send(&send, 1, MPI_INT, (rank+1) % size, 0, MPI_COMM_WORLD);
      send = recv;
    }
  }
  printf("Suma total: %d\n", sum);
  MPI_Finalize();
}
