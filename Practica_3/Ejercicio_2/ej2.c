#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#define SZ 5000

int min(int a, int b) {
  return (a < b ? a : b);
}

int main() {
  int arr[SZ];
  for (int i = 0; i < SZ; i++) {
    arr[i] = rand();
  }
  int m = arr[0];
  #pragma omp parallel for reduction(min: m)
  for (int i = 0; i < SZ; i++)
  {
    m = min(m, arr[i]);
  }
  printf("m: %d!\n", m);
  for (int i = 0; i < SZ; i++) {
    printf("%d ", arr[i]);
  }
  return 0;
}