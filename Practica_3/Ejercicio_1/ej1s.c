#include <stdio.h>
#include "timing.h"
#include <stdlib.h>

void tumama(double *sum, int size, double arr[]){
  for (int i = 0; i < size; i++)
    *sum = *sum + arr[i];
}

int main() {
  long int sz = 5e8;
  double sum = 0;
  float v;
  double *arr = malloc(sizeof(double) * sz);
  if (arr == NULL) perror("Error\n");
  for (int i = 0; i < sz; i++) {
    arr[i] = (double) 1;
  }
  TIME_void(tumama(&sum, sz, arr), NULL);

  printf("Sum: %f!\n", sum);
  printf("Exp: %ld!\n", sz);
  free(arr);
  return 0;
}
