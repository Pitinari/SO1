#include <stdio.h>
#include "timing.h"
#include <stdlib.h>
#include <omp.h>

float tumama(int size, double arr[]){
  float sum = 0;
  #pragma omp parallel for reduction (+:sum)
  for (int i = 0; i < size; i++) {
        sum = sum + arr[i];
  }
  return sum;
}

int main() {
  long int sz = 5e8;
  float sum = 0;
  double *arr = malloc(sizeof(double) * sz);
  if (arr == NULL) perror("Error\n");
  for (int i = 0; i < sz; i++) {
    arr[i] = (double) 1;
  }
  TIME(tumama(sz, arr), &sum);
  printf("Sum: %f!\n", sum);
  printf("Exp: %ld!\n", sz);
  free(arr);
  return 0;
}
