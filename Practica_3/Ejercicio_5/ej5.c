/* Particion de Lomuto, tomando el primer elemento como pivote */
int particionar(int a[], int N) {
  int i, j = 0;
  int p = a[0];
  swap(&a[0], &a[N-1]);
  for (i = 0; i < N-1; i++) {
    if (a[i] <= p)
    swap(&a[i], &a[j++]);
  }
  swap(&a[j], &a[N-1]);
  return j;
}

void qsort(int a[], int N) {
  if (N < 2)
    return;
  int p = particionar(a, N);
  qsort(a, p);
  qsort(a + p + 1, N - p -1);
}