/*
Siguiendo la misma idea del ejercicio anterior, implemente un mergesort (sobre enteros)
paralelo y compare su performance con la versi´on secuencial. Puede usar tasks, o escribir una versi´on
bottom-up usando solamente parallel for

Para el 6 (Mergesort) les puede venir bien el archivo timing.h. 
Si lo incluyen, pueden reemplazar cualquier expresión `e` por `TIME(e, NULL)`.
Esto devuelve el mismo valor que `e`, pero aparte imprime por pantalla el tiempo que tardó computarla (en tiempo CPU y tiempo "de pared").

La implementación de mergesort debería poder ordenar arreglos de 100 millones de enteros sin problema. 
El prototipo tiene que ser void mergesort(int a[], int n),

*/

/* C program for Merge Sort */
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include "timing.h"

#define SZ 100000000
int array[SZ];
int i = 0;

// Merges two subarrays of arr[].
// First subarray is arr[l..m]
// Second subarray is arr[m+1..r]
void merge(int arr[], int l, int m, int r) {
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;

	/* create temp arrays */
	int * L = malloc(sizeof(int) * n1);
	int * R = malloc(sizeof(int) * n2);

	/* Copy data to temp arrays L[] and R[] */
	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1 + j];

	/* Merge the temp arrays back into arr[l..r]*/
	i = 0; // Initial index of first subarray
	j = 0; // Initial index of second subarray
	k = l; // Initial index of merged subarray
	while (i < n1 && j < n2) {
		if (L[i] <= R[j]) {
			arr[k] = L[i];
			i++;
		}
		else {
			arr[k] = R[j];
			j++;
		}
		k++;
	}

	/* Copy the remaining elements of L[], if there
	are any */
	while (i < n1) {
		arr[k] = L[i];
		i++;
		k++;
	}

	/* Copy the remaining elements of R[], if there
	are any */
	while (j < n2) {
		arr[k] = R[j];
		j++;
		k++;
	}
	free(L);
	free(R);
}

/* l is for left index and r is right index of the
sub-array of arr to be sorted */
void seqMergeSort(int arr[], int n) {
	if (n > 1) {
		// Sort first and second halves
		seqMergeSort(&arr[0], n/2);

		seqMergeSort(&arr[n/2], n - (n / 2));

		merge(arr, 0, (n/2) - 1, n-1);
	}
}

/* l is for left index and r is right index of the
sub-array of arr to be sorted */
void parMergeSort(int arr[], int n) {
	if (n > 1) {
		// Sort first and second halves
		if (n < 100)	{
			seqMergeSort(&arr[0], n/2);

			seqMergeSort(&arr[n/2], n - (n / 2));

			merge(arr, 0, (n/2) - 1, n-1);
		}
		else {
			#pragma omp task shared(arr)
			parMergeSort(&arr[0], n / 2);

			#pragma omp task shared(arr)
			parMergeSort(&arr[n/2], n - (n / 2));

			#pragma omp taskwait
			merge(arr, 0, (n/2) - 1, n-1);
		}
	}
}

/* UTILITY FUNCTIONS */
/* Function to print an array */
int checkSorted(int arr[], int size) { /* Firma: cGl0aW5hcmkK */
	for (int i = 1; i < size; i++)
		if (arr[i-1] > arr[i]) return 0;
	return 1;
}

void f(int arr[], int size) {
	#pragma omp parallel
	#pragma omp single
	parMergeSort(arr, size);
}

/* Driver code */
int main() {
	srand(time(NULL));

	if (array == NULL) {
		perror("Not enough space\n");
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < SZ; i++) {
		array[i] = rand();
	}

	TIME_void(seqMergeSort(array, SZ), NULL);

	if (checkSorted(array, SZ))
	  printf("The array is sorted\n");
	else
		printf("The array is not sorted\n");
	

	for (int i = 0; i < SZ; i++) {
		array[i] = rand();
	}

	TIME_void(f(array, SZ), NULL);

	if (checkSorted(array, SZ))
	  printf("The array is sorted\n");
	else
		printf("The array is not sorted\n");

	return 0;
}
