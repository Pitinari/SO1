#include "unistd.h"
#include "stdlib.h"
#include "stdio.h"
#include "wait.h"

int main(int argc, char** argv){
  if(argc < 3){
    printf("Los argumentos no estan completos\n");
    return 0;
  }
  int wstatus;
  while(fork()){
    wait(&wstatus);
    printf("el proceso termino\n");
  }
  execl(argv[1], argv[1], NULL);
  return 0;
}
