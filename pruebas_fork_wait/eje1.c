#include "unistd.h"
#include "stdlib.h"
#include "stdio.h"

int main(int argc, char** argv){
  if(argc < 3){
    printf("Los argumentos no estan completos\n");
    return 0;
  }
  while(fork()){
    sleep(atoi(argv[2]));
  }
  execl(argv[1], argv[1], NULL);
  return 0;
}
