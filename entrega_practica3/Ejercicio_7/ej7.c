#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

void main(int argc, char **argv) {
  srand(time(0));
  int rank; // process id
  int sum; // suma del array
  int size; // numero de procesos
  int lenght = 99999; // largo del array
  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size); 
  
  int distro_Array[lenght]; // array
  int scattered_Data[lenght/size]; // array privado de the scattered data
  if (rank == 0) {
    // inicializa valores random
    for(int i = 0; i < lenght; ++i)
      distro_Array[i] = rand() % 10;
    
    // distribuye el array
    MPI_Scatter(&distro_Array, lenght/size, MPI_INT, &scattered_Data, lenght/size, MPI_INT, 0, MPI_COMM_WORLD);

    // suma la parte del array del root
    sum = 0;
    for (int i = 0; i < (lenght/size); i++) 
      sum += scattered_Data[i];

    // junta la informacion
    int buffer[size];
    MPI_Gather(&sum, 1, MPI_INT, buffer, 1, MPI_INT, 0, MPI_COMM_WORLD);
    sum = 0;

    // suma los sobrantes
    for (int i = 0; i < lenght % size; i++) 
      sum += distro_Array[(lenght/size)*size + i];

    // suma cada suma de los procesos
    for (int i = 0; i < size; i++)
      sum += buffer[i];
    printf("Suma total: %d\n", sum);
    int exp = 0;
    for (int i = 0; i < lenght; i++) {
      exp += distro_Array[i];
    }
    printf("Valor esperado: %d\n", exp);
  }
  else {
    MPI_Scatter(&distro_Array, lenght/size, MPI_INT, &scattered_Data, lenght/size, MPI_INT, 0, MPI_COMM_WORLD);
    sum = 0;
    for (int i = 0; i < (lenght/size); i++) 
      sum += scattered_Data[i];
    MPI_Gather(&sum, 1, MPI_INT, NULL, 0, MPI_INT, 0, MPI_COMM_WORLD);
  }
  
  MPI_Finalize();
}
