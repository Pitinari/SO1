#include <stdio.h>
#include <mpi.h>

void main(int argc, char **argv) {
  int rank, send = 1, recv, size, m;
  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  m = rank;
  send = rank;
  for (int i = 1; i < size; i *= 2) {
    MPI_Request rq;
    int socio;

    if (!(m % 2))
	    socio = rank + i;
    else
	    socio = rank - i;

    MPI_Isend(&send, 1, MPI_INT, socio, 0, MPI_COMM_WORLD, &rq);
    MPI_Recv(&recv, 1, MPI_INT, socio, 0, MPI_COMM_WORLD, &status);

    MPI_Wait(&rq, NULL);
    send += recv;
    m /= 2;
  }
  printf("Suma total: %d\n", send);
  MPI_Finalize();
}
