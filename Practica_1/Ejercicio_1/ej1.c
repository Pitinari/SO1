#include<stdlib.h> // malloc/realloc/free/exit
#include<stdio.h> // printf/fflush
#include<string.h> // strcmp
#include<unistd.h> // fork/close/dup/exevp
#include<sys/wait.h> // wait
#include<fcntl.h> //open/O_CREAT/O_WRONLY

const int INITIAL_ALLOC_STRING = 10;

char *stdin_dynamic (){
  char *string = malloc(INITIAL_ALLOC_STRING * sizeof(char));
  int cont = 0;
  char c;
  for(;(c = getchar()) != '\n'; cont++){
    if((cont % 10) == 0){
      string = realloc(string, cont+10);
    }
    string[cont] = c;
  }
  if((cont % 10) == 0){
    string = realloc(string, cont+1);
  }
  string[cont] = '\0';
  return string;
}

char *copy_word(int *i, int *j, char *string){
	char *word = malloc(((*j) - (*i) + 1)* sizeof(char));
	int idx = 0;
	if(string[*i] == ' '){
		(*i)++;
	}
	for(; (*i) < (*j); (*i)++){
		word[idx++] = string[*i];
	}
	word[idx] = '\0';
	return word;
}

char **command_and_args(char *line) { // recibe una cadena y la transforma en un arreglo de cadenas
	int idx = 0; // cabezal de la linea
	int lastSpace = 0; // posicion del inicio o del ultimo espacio
	int cant = 0; // cantidad de strings en el arreglo
	char **palabras = malloc(5 * sizeof(char*)); // arreglo de las cadenas
	while(line[idx] != '\0'){ // hasta encontrar el fin de la cadena
		if(line[idx] == ' '){ // si se encuentra un espacio
			if((idx - lastSpace) < 2 ) { // si el ultimo espacio y el indice diferencian en menos de 2 i.e. "  "
				lastSpace = idx; // cambiamos el ultimo espacio al indice
				idx++; // aumentamos el indice
				break;
			}
			if((cant % 5) == 0){ // si
				palabras = realloc(palabras, (cant+5) * sizeof(char*));
			}
			palabras[cant] = copy_word(&lastSpace, &idx, line);
			cant++;
		}
		idx++; // si no se encuentra un espacio se lee el siguiente caracter
	}

	if((cant % 5) == 0 || ((cant+1) % 5) == 0){
		palabras = realloc(palabras, (cant+2) * sizeof(char*));
	}
	if(line[idx-1] != ' '){
		palabras[cant] = copy_word(&lastSpace, &idx, line);
		palabras[cant+1] =  NULL;
	}
	else {
		palabras[cant] =  NULL;
	}
	return palabras;
}

void free_memory(char *line, char **commands){
	free(line);
	if(commands){
		char **command = commands;
		for(; *command; command++){
			free(*command);
		}
		free(commands);
	}
}

int main() {
	int wstatus;
	char *commandLine, **commandWithArgs;
	int fd = open("archivo.txt", O_CREAT | O_WRONLY, 0644);	

	while(1){
		printf(">> ");
		fflush(stdin);
		commandLine = stdin_dynamic();
		if(strcmp(commandLine, "exit") == 0){
			free_memory(commandLine, NULL);
			return 0;
		}
		commandWithArgs = command_and_args(commandLine);

		// TODO: change dir on cd chdir()
		pid_t pid1 = fork();
		if(!pid1){
			close(1); // close stdout
			dup(fd); // file to stdout
			execvp(commandWithArgs[0], commandWithArgs);
			exit(1);
		}
		wait(&wstatus);
		free_memory(commandLine, commandWithArgs);
	}
	return 1;
}