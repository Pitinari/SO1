#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

sem_t tabaco, papel, fosforos, otra_vez, turn[3];
int tabacoAux, papelAux, fosforosAux;

pthread_cond_t itemRecieved;
pthread_mutex_t lock;

struct item{
  sem_t *sem;
  int *counter;
};

struct item fosforosPointers = {&fosforos, &fosforosAux};
struct item tabacoPointers = {&tabaco, &tabacoAux};
struct item papelPointers = {&papel, &papelAux};


void *ins(void *arg) {
  for(;;) {
    pthread_mutex_lock(&lock);

    //usar waits
    pthread_cond_wait(&itemRecieved,&lock);
    puts("inspector");

    if (*(tabacoPointers.counter) && *(papelPointers.counter)) {
      *(tabacoPointers.counter)=0;
      *(papelPointers.counter)=0;
      sem_post(&turn[0]);
    }
    if (*(fosforosPointers.counter) && *(tabacoPointers.counter)) {
      *(fosforosPointers.counter)=0;
      *(tabacoPointers.counter)=0;
      sem_post(&turn[1]);
    }
    if (*(papelPointers.counter) && *(fosforosPointers.counter)) {
      *(tabacoPointers.counter)=0;
      *(fosforosPointers.counter)=0;
      sem_post(&turn[2]);
    }
    pthread_mutex_unlock(&lock);
  }
}

void *listener(void *arg) {
  struct item aux = *((struct item*)arg);
  printf("%i\n", aux.sem);
  for(;;){
    sem_wait(aux.sem);
    (*(aux.counter))++;
    //SIGNAL
    pthread_cond_signal(&itemRecieved);
  }
}

void agente() {
  for (;;) {
    int caso = random() % 3;
    sem_wait(&otra_vez);
    switch (caso) {
      case 0:
        sem_post(&tabaco);
        sem_post(&papel);
        break;
      case 1:
        sem_post(&fosforos);
        sem_post(&tabaco);
        break;
      case 2:
        sem_post(&papel);
        sem_post(&fosforos);
        break;
    }
  }
}

void fumar(int fumador) {
  printf("Fumador %d: Puf! Puf! Puf!\n", fumador);
  sleep(1);
}

void *fumador1(void *arg) {
  for (;;) {
    sem_wait(&turn[0]);
    fumar(1);
    sem_post(&otra_vez);
  }
}

void *fumador2(void *arg){
  for (;;) {
    sem_wait(&turn[1]);
    fumar(2);
    sem_post(&otra_vez);
  }
}

void *fumador3(void *arg) {
  for (;;) {
    sem_wait(&turn[2]);
    fumar(3);
    sem_post(&otra_vez);
  }
}

int main() {
  pthread_t s1, s2, s3, listeners[3], inspe;
  sem_init(turn, 0, 0);
  sem_init(turn+1, 0, 0);
  sem_init(turn+2, 0, 0);
  sem_init(&tabaco, 0, 0);
  sem_init(&papel, 0, 0);
  sem_init(&fosforos, 0, 0);
  sem_init(&otra_vez, 0, 1);

  printf("%i\n",&fosforos );
  printf("%i\n",&tabaco );
  printf("%i\n",&papel );

  pthread_create(&s1, NULL, fumador1, NULL);
  pthread_create(&s2, NULL, fumador2, NULL);
  pthread_create(&s3, NULL, fumador3, NULL);
  pthread_create(&inspe, NULL, ins, NULL);
  pthread_create(listeners, NULL, listener, (void*)&fosforosPointers);
  pthread_create(listeners+1, NULL, listener, (void*)&tabacoPointers);
  pthread_create(listeners+2, NULL, listener, (void*)&papelPointers);
  agente();
  return 0;
}

/*¿Cómo puede ocurrir un deadlock?
Si F1 toma el tabaco y F3 le toma el papel,
Si F2 toma los fósforos y F1 le toma el tabaco,
Si F3 toma papel y F2 le toma los fósforos,
*/


/*Implemente una solución y explíquela.*/
