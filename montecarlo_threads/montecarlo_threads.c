#include<pthread.h>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>

#define N_THREADS 6
#define ITERATIONS_PER_THREAD 3000000
#define RADIUS 5

int outCircleDots[N_THREADS] = {0};
pthread_t threadsIds[N_THREADS] = {0};

float rand_float(int range){
    int randInt = rand();
    return ((float)randInt / RAND_MAX) * (float)range;
}

void *dottingSquare(void *idx){

    for(int _ = 0; _<ITERATIONS_PER_THREAD; _++){
        float x = rand_float(RADIUS*2) - RADIUS;
        float y = rand_float(RADIUS*2) - RADIUS;
        if((x*x + y*y) > (float)RADIUS*RADIUS){
            outCircleDots[(int)idx]++;
        }
    }
    return NULL;
}

int main(){
    srand(time(NULL));
    for(int i = 0; i < N_THREADS; i++){
        pthread_create(&threadsIds[i], NULL, dottingSquare, (void*)i);
    }

    for(int i = 0; i < N_THREADS; i++){
        pthread_join(threadsIds[i], NULL);
    }

    int outOfCircle = 0;
    for(int i = 0; i < N_THREADS; i++){
        outOfCircle += outCircleDots[i];
    }

    int square = N_THREADS * ITERATIONS_PER_THREAD;
    int circle = square - outOfCircle;

    printf("square dots: %d\ncircle dots: %d\nPi aproximation: %f\n",square, circle, ((float)circle / (float)square)*4);

    return 0;
}