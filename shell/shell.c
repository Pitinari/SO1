#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<limits.h>
#include <sys/types.h>
#include <sys/wait.h>

const int INITIAL_ALLOC_STRING = 10;

char *stdin_dynamic (){
    char *string = malloc(INITIAL_ALLOC_STRING * sizeof(char));
    int cont = 0;
    char c;
    for(;(c = getchar()) != '\n'; cont++){
        if((cont % 10) == 0){
            string = realloc(string, cont+10);
        }
        string[cont] = c;
    }
    if((cont % 10) == 0){
        string = realloc(string, ++cont);
    }
    string[cont] = '\0';
    return string;
}

char *copy_word(int *i, int *j, char *string){
    char *word = malloc(((*j) - (*i) + 1)* sizeof(char));
    int idx = 0;
    if(string[*i] == ' '){
        (*i)++;
    }
    for(; (*i) < (*j); (*i)++){
        word[idx++] = string[*i];
    }
    word[idx] = '\0';
    return word;
}

char **command_and_args(char *line){
    int idx = 0, lastSpace = 0, cant = 0;
    char **palabras = malloc(5 * sizeof(char*));
    while(line[idx] != '\0'){
        if(line[idx] == ' '){
            if((idx - lastSpace) < 2 ){
                lastSpace = idx;
                idx++;
                break;
            }
            if((cant % 5) == 0){
                palabras = realloc(palabras, cant+5);
            }
            palabras[cant] = copy_word(&lastSpace, &idx, line);
            cant++;
        }
        idx++;
    }
    if((cant % 5) == 0 || ((cant+1) % 5) == 0){
        palabras = realloc(palabras, cant+2);
    }
    if(line[idx-1] != ' '){
        palabras[cant] = copy_word(&lastSpace, &idx, line);
        palabras[cant+1] =  NULL;
    }
    else {
        palabras[cant] =  NULL;
    }
    return palabras;
}

void free_memory(char *line, char **commands){
    free(line);
    if(commands){
        for(int idx = 0; commands[idx]; idx++){
            free(commands[idx]);
        }
        free(commands);
    }
}

int main() {
    int wstatus;
    char *commandLine, **commandWithArgs;
    while(1){
        printf(">> ");
        fflush(stdin);
        commandLine = stdin_dynamic();
        if(strcmp(commandLine, "exit") == 0){
            free_memory(commandLine, NULL);
            return 0;
        }
        commandWithArgs = command_and_args(commandLine);
        // TODO: change dir on cd chdir()
        pid_t pid = fork();
        if(!pid){
            execv(commandWithArgs[0], commandWithArgs);
            exit(1);
        }
        wait(&wstatus);
        free_memory(commandLine, commandWithArgs);
    }
    return 1;
}
