-module(server).
-export([init_server/0, serv/0, wait_connect/2, handle_conn/3, request_parser/3, tickets/1]).

init_server() ->
    spawn(?MODULE, serv, []).

serv() ->
    case gen_tcp:listen(3000, [{reuseaddr, true}, {active, false}]) of
            {error, Reason} -> {error, Reason};
            {ok, ListenSocket} -> io:fwrite("SERVER CREATED~n"),
                                  TicketerID = spawn(?MODULE, tickets, [0]), % Hilo para manejar el numero a devolver en el socket
        	                      wait_connect(ListenSocket, TicketerID)
    end.


wait_connect(ListenSocket, TicketerID) ->
	{ok, Socket} = gen_tcp:accept(ListenSocket),
    io:fwrite("SOCKET CREATED~n", []),
	spawn(?MODULE, handle_conn, [Socket, TicketerID, ""]),  % Levanta un hilo para el socket 
    wait_connect(ListenSocket, TicketerID). % Escucha por mas conexiones

handle_conn(Sock, TicketerID, AccumulatedRequest) ->    % AccumulatedRequest mantiene los mensajes anteriores que no completaron una "linea"
    case gen_tcp:recv(Sock, 0) of
        {ok, Msg} ->    io:fwrite("MESSAGE RECEIVED ~p~n",[Msg]),
                        handle_conn(Sock, TicketerID, request_parser(Sock, TicketerID, AccumulatedRequest ++ Msg)); % Inicia el loop despues de consumir todos los posibles mensajes
        {error, enotconn} ->    io:fwrite("SOCKET CERRADO~n"), % Cierra el socket en caso de no haber conexion
                                gen_tcp:close(Sock);
        {error, closed} ->  io:fwrite("SOCKET CERRADO~n"),  % Cierra el socket en caso de haberse cerrado
                            gen_tcp:close(Sock);
        _ ->    handle_conn(Sock, TicketerID, AccumulatedRequest)   % Cualquier otro mensaje se ignora
    end.

request_parser(Sock, TicketerID, Request) ->
    ParsedRequest = string:split(Request, "\n"), % Separa la primera linea de un string
    % Ejemplo: split("hola\nchau","\n") => ["hola","chau"]
    %          split("ho", "\n") => ["ho"]
    case length(ParsedRequest) of
        2 ->    Line = lists:nth(1,ParsedRequest), % En el caso de haber al menos una linea
                case Line of
                    "NUEVO" ->   TicketerID ! {nuevo, self()},  % En caso de ser NUEVO se pide un numero unico y se manda
                                receive
                                    {ok, N} -> gen_tcp:send(Sock, list_to_binary(integer_to_list(N)));
                                    _ -> error
                                end;
                    "CHAU" ->   gen_tcp:close(Sock),    % En caso de de CHAU cierra el socket
                                io:fwrite("SOCKET CERRADO~n"),
                                exit("SOCKET CERRADO"); % Luego mata el hilo
                    _ -> io:fwrite("MENSAJE ERRONEO~n") % Cualquier otro mensaje lo consume e ignora
                end,
                LeftoverRequest = lists:nth(2,ParsedRequest),
                ReturnMsg = request_parser(Sock, TicketerID, LeftoverRequest); % Vuelve a consumir en mensaje hasta agotarlo, esto es para el caso de que el mensaje llegue como "NUEVO\nNUEVO\n"
        1 ->    io:fwrite("MENSAJE INCOMPLETO~n"),
                ReturnMsg = Request % En caso de que la request no complete una linea o no quede mas request
    end,
    ReturnMsg. % Devuelve el resto de 

tickets(N) ->
    receive
        {nuevo, PID} -> PID ! {ok, N}, % Escucha a quien pida un numero unico
                        tickets(N+1);
        _ -> error
    end.
