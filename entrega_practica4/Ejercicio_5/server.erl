-module(server).
-export([iniciar_servidor/0, servidor/1, cliente/1]).

servidor(List) ->
  receive
    {susc, PId} -> case lists:member(PId, List) of
                      true -> io:fwrite("Ya suscrito ~p~n", [PId]),
                              servidor(List);
                      false -> io:fwrite("Suscrito ~p~n", [PId]),
                               servidor([PId] ++ List)
                   end;
    {mensaje, MSG} -> lists:foreach(fun(A) -> A ! MSG end, List),
                      servidor(List);
    {desusc, PId} -> servidor(lists:delete(PId, List));
    _ -> servidor(List)
  end.

cliente(Server) ->
  Server !  {susc, self()},
  Server !  {desusc, self()},
  Server !  {susc, self()},
  receive
    after 100 -> io:fwrite("Esperando...~n")
  end,
  Server !  {mensaje, self()},
  receive
    A -> io:fwrite("Mensaje: ~p ~p~n",[A, self()])
  end,
  receive
    B -> io:fwrite("Mensaje: ~p ~p~n",[B, self()])
  end.


iniciar_servidor() ->
  Server = spawn(?MODULE, servidor, [[]]),
  spawn(?MODULE, cliente, [Server]),
  spawn(?MODULE, cliente, [Server]).
