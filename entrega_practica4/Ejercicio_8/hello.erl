-module(hello).
-export([init/0, hello_aux/1, hello/0, init_aux/0]).

hello_aux(PId) ->
  link(PId),
  hello().

hello() ->
  receive after 1000 -> ok end,
  io:fwrite("hello ~p~n", [case rand:uniform(10) of 10 -> 1/uno; _ -> self() end]),
  hello().

revive() ->
  receive
    {'EXIT', From, _Reason} -> spawn(?MODULE, hello_aux, [self()]),
                               io:fwrite("process: ~p~n", [From]),
                               revive()
  end.

init_aux() -> 
  spawn(?MODULE, hello_aux, [self()]),
  process_flag(trap_exit, true),
  revive().

init() -> 
  spawn(?MODULE, init_aux, []).